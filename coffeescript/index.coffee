# Imports.
net              = require('net')
JSONStream       = require('JSONStream')
Car              = require('./app/cars/car')
Track            = require('./app/tracks/track')
AiCarController  = require('./app/ai/ai-car-controller')


# Env vars.
serverHost = process.argv[2]
serverPort = process.argv[3]
botName = process.argv[4]
botKey = process.argv[5]


console.log "I'm", botName, "and connect to", serverHost + ":" + serverPort


# Client.
client = net.connect serverPort, serverHost, () ->
  console.log 'connected'
  send {msgType: "join", data: {name: botName, key: botKey}}
#  send {
#    msgType: 'createRace'
#    data: {
#      botId: {
#        name: botName
#        key: botKey
#      }
#      trackName: 'germany'
#      password: 'magic'
#      carCount: 1
#    }
#  }


# Send.
send = (json) ->
  if tick != -1
    json.gameTick = tick
#  console.log '->', JSON.stringify json
  client.write JSON.stringify json
  client.write '\n'


ping = () ->
  send {msgType: "ping", data: {}}


# Stream.
jsonStream = client.pipe(JSONStream.parse())


# AI.
tick = -1
myCar = null
opponents = []
carsById = {}
track = null
ai = null


# Returns car by its name and color.
getCar = (name, color) ->
  carsById[name + '_' + color]


# Stream data.
jsonStream.on 'data', (data) ->
  if 'gameTick' of data
    tick = data.gameTick
  msgType = data.msgType
  data = data.data

  if msgType == 'carPositions'
    onCarPositions data
  else
    switch msgType
      when 'join'
        onJoin data
      when 'yourCar'
        onYourCar data
      when 'gameInit'
        onGameInit data
      when 'gameStart'
        onGameStart data
      when 'crash'
        onCrash data
      when 'spawn'
        onSpawn data
      when 'lapFinished'
        onLapFinished data
      when 'gameEnd'
        onGameEnd data
      when 'tournamentEnd'
        onTournamentEnd data
      else
        onUnknown msgType, data


# Join handler.
onJoin = (data) ->
  console.log 'join'
  ping()
  null


# Your car handler.
onYourCar = (data) ->
  console.log 'your car'
  myCar = new Car data.name, data.color
  carsById[myCar.id] = myCar
  ping()
  null


# Game init handler.
onGameInit = (data) ->
  console.log 'game init'
  # Create track.
  track = new Track data.race.track
  sessionInfo = data.race.raceSession
  # Add cars.
  for c in data.race.cars
    if c.id.color == myCar.color
      # Own car.
      myCar.length = Number c.dimensions.length
      myCar.width = Number c.dimensions.width
      myCar.guideFlagPosition = Number c.dimensions.guideFlagPosition
    else
      # Opponent.
      o = new Car c.id.name, c.id.color, Number c.dimensions.length, Number c.dimensions.width, Number c.dimensions.guideFlagPosition
      carsById[o.id] = o
      opponents.push o
  # Create AI.
  ai = new AiCarController myCar, opponents, track, sessionInfo
  ping()
  null


# Game start handler.
onGameStart = (data) ->
  console.log 'game start'
  ping()
  null


# Car positions handler.
onCarPositions = (data) ->
  progress = 0
  # Set car positions.
  for c in data
    car = getCar(c.id.name, c.id.color)
    car.setPositionData c
    if car == myCar
      progress = c.piecePosition.pieceIndex / track.pieces.length
  console.log '[p] [' + tick + ']', progress
  # Run AI.
  ai.tick()
  # Send car output.
  send myCar.output
  null


# Car crash handler.
onCrash = (data) ->
  console.log '******************************* crash *******************************'
  car = getCar(data.name, data.color)
  car.controller.notifyCrash()
  null


# Car spawn handler.
onSpawn = (data) ->
  console.log '+++++++++++++++++++++++++++++++ spawn +++++++++++++++++++++++++++++++'
  car = getCar(data.name, data.color)
  car.controller.notifySpawn()
  null


# Lap finished handler.
onLapFinished = (data) ->
  # TODO
  null


# Game end handler.
onGameEnd = (data) ->
  console.log 'game end'
  ping()
  null


# Tournament end handler.
onTournamentEnd = (data) ->
  # TODO:
  null


# Unknown handler.
onUnknown = (msgType, data) ->
  console.log 'unknown', msgType, data
  ping()
  null


# Stream error.
jsonStream.on 'error', ->
  console.log 'disconnected'
