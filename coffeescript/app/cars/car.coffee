class Car

  # Car constructor.
  constructor: (@name, @color, @length = 40.0, @width = 20.0, @guideFlagPosition = 10.0) ->
    @id = @name + '_' + @color
    @angle = 0
    @currentSpeed = 0
    @currentAcceleration = 0
    @damping = 1
    @acceleration = 0.001
    @friction = 21.56
    @staticFriction = 0
    @dynamicFriction = 0
    @crashAngle = 60
    @destSpeed = 0
    @throttle = 0.5
    @piecePosition =
      pieceIndex: 0
      inPieceDistance: 0
      lane:
        startLaneIndex: 0
        endLaneIndex: 0
      lap: 0
    @crashed = false
    @controller = null
    @output = null
    @setOutput 'ping'


  # Sets position from server data.
  setPositionData: (data) ->
    @angle = data.angle
    @piecePosition.pieceIndex = data.piecePosition.pieceIndex
    @piecePosition.inPieceDistance = data.piecePosition.inPieceDistance
    @piecePosition.lane.startLaneIndex = data.piecePosition.lane.startLaneIndex
    @piecePosition.lane.endLaneIndex = data.piecePosition.lane.endLaneIndex
    @piecePosition.lap = data.piecePosition.lap
    null


  # Sets car output to be sent to the server.
  setOutput: (type, data) ->
    if type == 'throttle'
      @output =
        msgType: 'throttle'
        data: Math.min(Math.max(data || 0, 0), 1)
    else if type == 'switchLane' and data in ['Left', 'Right']
      @output =
        msgType: 'switchLane'
        data: data
    else
      @output =
        msgType: 'ping'
    null


# Class export.
module.exports = Car
