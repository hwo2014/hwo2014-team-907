class AiCarController

  MAX_DAMPING_DIFF = 1 / 10000
  MAX_ACCELERATION_DIFF = 1 / 10000
  DAMPING_CONSTANT = true
  ACCELERATION_CONSTANT = true
  MAX_SPEED = 99999
  SAFE_ANGLE_MARGIN = 0

  # Calculates path length.
  calculatePathLength = (pieces, path, fromPieceIndex, fromLaneIndex) ->
    length = pieces[fromPieceIndex].lengths[fromLaneIndex]
    for laneIndex, i in path
      length += pieces[fromPieceIndex + i].lengths[laneIndex]
    length


  # Finds the most optimal path.
  findMostOptimalPath = (pieces, lanes, fromPieceIndex, fromLaneIndex) ->
    findShortestPath pieces, lanes, fromPieceIndex, fromLaneIndex


  # Finds the shortest path.
  findShortestPath = (pieces, lanes, fromPieceIndex, fromLaneIndex) ->
    paths = findAllPossiblePaths pieces, lanes, fromPieceIndex, fromLaneIndex
    pathsWithLength = []
    for path in paths
      pathsWithLength.push {path: path, length: calculatePathLength(pieces, path, fromPieceIndex, fromLaneIndex)}
    sorted = pathsWithLength.sort (a, b) ->
      a.length - b.length
    sorted[0]


  # Finds all possible paths (lanes), starting from current piece. In a form of a tree (nested array).
  findAllPossiblePaths = (pieces, lanes, fromPieceIndex, fromLaneIndex, allPaths = [], currentPath = []) ->
    if fromPieceIndex < pieces.length
      nextLanes = [fromLaneIndex]
      if pieces[fromPieceIndex].switch
        if fromLaneIndex > 0
          nextLanes.push fromLaneIndex - 1
        if fromLaneIndex < lanes.length - 1
          nextLanes.push fromLaneIndex + 1
      for lane in nextLanes
        nextPath = currentPath.concat [lane]
        findAllPossiblePaths pieces, lanes, fromPieceIndex + 1, lane, allPaths, nextPath
      return allPaths
    else
      allPaths.push currentPath


  # Constructor.
  constructor: (@car = null, @opponents = null, @track = null, @session) ->
#    console.log JSON.stringify @track, null, 4
    @car.controller = @
    @cars = @opponents.concat [@car]
    @lastPiecePosition = null
    @laneSwitch =
      cache: false
      shortestPathCache: []
      lastToLane: null
      lastFromLane: null
      lastOnPiece: null
    @car.turnSpeed = 5
    @computeTrackSpeed()


  # Notifies the car about a next simulation tick.
  tick: () ->
    @computeParams()
#    @computeFriction()
    @computeTrackSpeed()
    @computeDestSpeed()
    @computeThrottle()
#    console.log '[' + @car.piecePosition.pieceIndex + '] [' + @car.piecePosition.inPieceDistance + '] speed:', @car.currentSpeed, 'damping:', @car.damping, 'acceleration:', @car.acceleration
    console.log 'speed:', @car.currentSpeed.toFixed(3), 'throttle:', @car.throttle, 'friction:', @car.friction, 'crashAngle:', @car.crashAngle, 'angle:', @car.angle
#    console.log 'speed:', @car.currentSpeed.toFixed(3), '\t\tangle:', @car.angle, '\t\tCforce:', @car.currentCentrifugalForce #, '\t\tSfriction:', @car.staticFriction
    if not @switchLanes()
      @car.setOutput 'throttle', @car.throttle

    for car in @cars
      car.last.angle = car.angle
    null


  # Computes parameters like acceleration and velocity for all cars.
  computeParams: () ->
    for car in @cars
      # Calculate params
      isSpeedValid = true
      if car.last # for some weird reason speed jumps down a bit when switching pieces from straight to bend
        isSpeedValid = car.last.piecePosition.pieceIndex == car.piecePosition.pieceIndex or @track.pieces[car.last.piecePosition.pieceIndex].type == @track.pieces[car.piecePosition.pieceIndex].type

        # Speed
        distanceTraveled = 0
        if car.piecePosition.pieceIndex == car.last.piecePosition.pieceIndex
          distanceTraveled = car.piecePosition.inPieceDistance - car.last.piecePosition.inPieceDistance
        else
          # Previous piece until end.
          distanceTraveled = @track.pieces[car.last.piecePosition.pieceIndex].lengths[car.last.piecePosition.lane.endLaneIndex] - car.last.piecePosition.inPieceDistance
          # Next piece (TODO: if traveled more than one piece?)
          distanceTraveled += car.piecePosition.inPieceDistance
        car.currentSpeed = distanceTraveled

        # Current acceleration.
        if not car.crashed
          car.currentAcceleration = car.currentSpeed - car.last.currentSpeed

        # Centrifugal force / centripetal acceleration (M = 1)
        car.currentCentrifugalForce = 0
        if @track.pieces[car.piecePosition.pieceIndex].type == 'bend'
          car.currentCentrifugalForce = car.currentSpeed * car.currentSpeed / @track.pieces[car.piecePosition.pieceIndex].radiuses[car.piecePosition.lane.endLaneIndex]

        # Static friction.
        if car.angle == 0
          if car.currentCentrifugalForce > car.staticFriction
            car.staticFriction = car.currentCentrifugalForce
        else if car.last.angle == 0
          if car.currentCentrifugalForce < car.staticFriction
            car.staticFriction = car.currentCentrifugalForce

        # Dynamic friction.


        # Damping.
        if not car.dampingComputed and not car.crashed and car.throttle == car.last.throttle and car.last.currentAcceleration
          car.damping = car.currentAcceleration / car.last.currentAcceleration
          if DAMPING_CONSTANT and car.damping and Math.abs(car.damping - car.last.damping) < MAX_DAMPING_DIFF
            car.dampingComputed = true
            console.log JSON.stringify @track, null, 4
            console.log @car.width, @car.length, @car.guideFlagPosition

        # Acceleration (engine power capabilities).
        if not car.accelerationComputed and not car.crashed and car.throttle == car.last.throttle and car.last.currentAcceleration
          car.acceleration = (car.currentSpeed / car.damping - car.last.currentSpeed) / car.throttle
          if ACCELERATION_CONSTANT and car.acceleration and Math.abs(car.acceleration - car.last.acceleration) < MAX_ACCELERATION_DIFF
            car.accelerationComputed = true

      # Store last params.
      car.last =
        angle: if car.last then car.last.angle else 0
        currentSpeed: car.currentSpeed
        currentAcceleration: car.currentAcceleration
        damping: car.damping
        acceleration: car.acceleration
        throttle: car.throttle
        piecePosition:
          pieceIndex: car.piecePosition.pieceIndex
          inPieceDistance: car.piecePosition.inPieceDistance
          lane:
            startLaneIndex: car.piecePosition.lane.startLaneIndex
            endLaneIndex: car.piecePosition.lane.endLaneIndex
          lap: car.piecePosition.lap
    null


  # Computes friction based on slip angle.
  computeFriction: () ->
    # Check if currently on a bend.
    piece = @track.pieces[@car.piecePosition.pieceIndex]
    laneIndex = @car.piecePosition.lane.endLaneIndex
    if piece.type == 'bend' and @car.angle != 0
      total = piece.totalLengths[laneIndex]
      remaining = piece.remainingLengths[laneIndex] - @car.piecePosition.inPieceDistance
      traveled = total - remaining
      progress = traveled / total
      expectedAngle = @car.crashAngle * progress * (1 - SAFE_ANGLE_MARGIN)
      @car.friction *= @car.angle / expectedAngle
      console.log '[ANGLE]', @car.angle, expectedAngle, @car.friction
    null


  # Computes safe speed for each piece of the track.
  computeTrackSpeed: () ->
    # First, compute speeds ignoring slow down time.
    for piece in @track.pieces
      piece.speeds = piece.speeds || []
      for lane, laneIndex in @track.lanes
        piece.speeds[laneIndex] = @computePieceSpeed piece, lane, laneIndex

    # Then go backwards and adjust each piece's time based on required slow down time.
    if @track.pieces.length > 1
      for i in [0..(@track.pieces.length * 2)]
        index = i % @track.pieces.length
        nextIndex = (i + 1) % @track.pieces.length
        piece = @track.pieces[index]
        nextPiece = @track.pieces[nextIndex]
        for lane, laneIndex in @track.lanes
          s = piece.lengths[laneIndex]
          v = nextPiece.speeds[laneIndex]
          while s > 0
            v /= @car.damping
            s -= v
          piece.speeds[laneIndex] = Math.min v, piece.speeds[laneIndex]
    null


  # Computes safe speed for a given piece, taken its angle and length.
  computePieceSpeed: (piece, lane, laneIndex) ->
    curve = piece.curves[laneIndex]
    if curve == 0
      return MAX_SPEED
    else
      return Math.sqrt(@car.friction / curve)
    null


  computeDestAngle: () ->
    piece = @track.pieces[@car.piecePosition.pieceIndex]
    laneIndex = @car.piecePosition.lane.endLaneIndex
    remaining = piece.remainingLengths[laneIndex] + piece.lengths[laneIndex] - @car.piecePosition.inPieceDistance
    total = piece.totalLengths[laneIndex]
    driven = total - remaining
    progress = driven / total
    Math.sqrt(Math.pow(@car.crashAngle, 2) * progress)


  # Computes desired speed.
  computeDestSpeed: () ->
    laneIndex = @car.piecePosition.lane.endLaneIndex
    nextPieceIndex = (@car.piecePosition.pieceIndex + 1) % @track.pieces.length
    piece = @track.pieces[@car.piecePosition.pieceIndex]
    nextPiece = @track.pieces[nextPieceIndex]
    speed = piece.speeds[laneIndex]
    progress = @car.piecePosition.inPieceDistance / piece.lengths[laneIndex]
    nextSpeed = nextPiece.speeds[laneIndex]
    angleRatio = Math.abs(@car.angle) / @car.crashAngle
#    if Math.abs(@car.angle) > Math.abs(@car.last.angle)
    @car.destSpeed = speed + (nextSpeed - speed) * progress
#    else
#      @car.destSpeed = Math.max(speed + (nextSpeed - speed) * progress, nextSpeed)
    null


  # Adjusts throttle to desired speed.
  computeThrottle: () ->
    newThrottle = ((@car.destSpeed / @car.damping) - @car.currentSpeed) / @car.acceleration
    @car.throttle = Math.min(Math.max(newThrottle, 0), 1)
    null


  # Tries to switch lanes if needed.
  switchLanes: () ->
    shortestPath = null
    direction = null
    toIndex = null

    # Compute shortest path.
    if @laneSwitch.cache
      if not @laneSwitch.shortestPathCache[@car.piecePosition.pieceIndex]
        @laneSwitch.shortestPathCache[@car.piecePosition.pieceIndex] = []
      if not @laneSwitch.shortestPathCache[@car.piecePosition.pieceIndex][@car.piecePosition.lane.startLaneIndex]
        @laneSwitch.shortestPathCache[@car.piecePosition.pieceIndex][@car.piecePosition.lane.startLaneIndex] = findShortestPath @track.pieces, @track.lanes, @car.piecePosition.pieceIndex, @car.piecePosition.lane.startLaneIndex
      shortestPath = @laneSwitch.shortestPathCache[@car.piecePosition.pieceIndex][@car.piecePosition.lane.startLaneIndex]
    else
      shortestPath = findShortestPath @track.pieces, @track.lanes, @car.piecePosition.pieceIndex, @car.piecePosition.lane.startLaneIndex

    # See if change is possible.
    if shortestPath.path.length > 0 and shortestPath.path[0] != @car.piecePosition.lane.startLaneIndex
      direction = if shortestPath.path[0] > @car.piecePosition.lane.startLaneIndex then 'Right' else 'Left'
      toIndex = shortestPath.path[0]
    else if shortestPath.path.length > 1 and shortestPath.path[1] != shortestPath.path[0] and shortestPath.path[1] != @car.piecePosition.lane.startLaneIndex
      direction = if shortestPath.path[1] > @car.piecePosition.lane.startLaneIndex then 'Right' else 'Left'
      toIndex = shortestPath.path[1]
    if direction and @laneSwitch.lastOnPiece != @car.piecePosition.pieceIndex
      @laneSwitch.lastToLane = toIndex
      @laneSwitch.lastOnPiece = @car.piecePosition.pieceIndex
      @car.setOutput 'switchLane', direction
      console.log 'switching lanes to', direction
      return true
    false


  # When the car has crashed.
  notifyCrash: () ->
    if @track.pieces[@car.piecePosition.pieceIndex].type == 'bend'
      remaining = @track.pieces[@car.piecePosition.pieceIndex].remainingLengths[@car.piecePosition.lane.endLaneIndex] + @track.pieces[@car.piecePosition.pieceIndex].lengths[@car.piecePosition.lane.endLaneIndex] - @car.piecePosition.inPieceDistance
      total = @track.pieces[@car.piecePosition.pieceIndex].totalLengths[@car.piecePosition.lane.endLaneIndex]
      driven = total - remaining
      progress = driven / total
      curve = @track.pieces[@car.piecePosition.pieceIndex].curves[@car.piecePosition.lane.endLaneIndex]
      safeSpeed = Math.pow(progress, 1/2) * @car.last.currentSpeed * 1.02
      friction = safeSpeed * safeSpeed * Math.abs(curve)
      @car.friction = friction
    else
      @car.friction *= 0.9
    @car.crashAngle = @car.angle
    console.log '/////////// CRASH'
    console.log 'piece:', @track.pieces[@car.piecePosition.pieceIndex]
    console.log 'totalLength:', @track.pieces[@car.piecePosition.pieceIndex].totalLengths[@car.piecePosition.lane.endLaneIndex]
    console.log 'progress:', progress
    console.log 'speed:', @car.last.currentSpeed
    console.log 'angle:', @car.angle
    console.log 'curve:', curve
    console.log 'safeSpeed:', safeSpeed
    console.log 'safeFriction:', friction
    null


  # When the car has spawned.
  notifySpawn: () ->
    null


# Class export.
module.exports = AiCarController
