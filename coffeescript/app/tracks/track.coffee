class Track

  # Calculates length for bend pieces.
  calculateBendLength = (radius, angle) ->
    Math.abs 2 * Math.PI * radius * angle / 360


  # Constructor.
  constructor: (data) ->
    # Transcribe keys.
    for key, value of data
      @[key] = value

    # Calculate bend lengths.
    for piece in @pieces
      piece.lengths = []
      piece.curves = []
      piece.totalAngles = []
      piece.totalCurves = []
      piece.radiuses = []
      piece.avgRadiuses = []
      piece.totalLengths = []
      piece.remainingLengths = []
      for lane in @lanes
        # Calculate lengths, curves and curve distances.
        if piece.radius and piece.angle
          piece.type = 'bend'
          piece.radiuses.push piece.radius + (if piece.angle > 0 then -lane.distanceFromCenter else lane.distanceFromCenter)
          piece.lengths.push(calculateBendLength piece.radius + (if piece.angle > 0 then -lane.distanceFromCenter else lane.distanceFromCenter), piece.angle)
          piece.curves.push Math.abs(piece.angle) / (piece.radius + (if piece.angle > 0 then -lane.distanceFromCenter else lane.distanceFromCenter))
        else
          piece.type = 'straight'
          piece.lengths.push piece.length
          piece.totalAngles.push 0
          piece.radiuses.push 0
          piece.avgRadiuses.push 0
          piece.curves.push 0
          piece.totalCurves.push 0
          piece.totalLengths.push piece.length
          piece.remainingLengths.push piece.length

    # Calculate curve lengths.
    i = 0
    bendPieces = []
    while i < @pieces.length
      piece = @pieces[i]
      if piece.type == 'bend'
        pieces = [piece]
        for j in [(i + 1)..(@pieces.length - 1)]
          if j < @pieces.length and @pieces[j].type == 'bend' and @pieces[j].angle * piece.angle > 0
            pieces.push @pieces[j]
          else
            bendPieces.push pieces
            break
        i += pieces.length - 1
      ++i
    for pieces in bendPieces
      for lane, laneIndex in @lanes
        totalLength = 0
        avgRadius = 0
        totalAngle = 0
        totalCurve = 0
        for piece in pieces
          totalLength += piece.lengths[laneIndex]
          totalAngle += piece.angle
          avgRadius += piece.radiuses[laneIndex]
        avgRadius /= pieces.length
        for piece, pieceIndex in pieces
          piece.totalLengths[laneIndex] = totalLength
          for i in [(pieceIndex+1)..(pieces.length - 1)]
            if i > pieceIndex and i < pieces.length
              piece.remainingLengths[laneIndex] = piece.remainingLengths[laneIndex] || piece.lengths[laneIndex]
              piece.remainingLengths[laneIndex] += pieces[i].lengths[laneIndex]
          piece.remainingLengths[laneIndex] = piece.remainingLengths[laneIndex] || 0
          piece.avgRadiuses[laneIndex] = avgRadius
          piece.totalAngles[laneIndex] = totalAngle
          piece.totalCurves[laneIndex] = Math.abs(totalAngle) / avgRadius

# Class export.
module.exports = Track
